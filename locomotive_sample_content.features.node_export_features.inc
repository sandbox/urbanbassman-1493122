<?php
/**
 * @file
 * locomotive_sample_content.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function locomotive_sample_content_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  array(
    \'vid\' => \'1\',
    \'uid\' => \'1\',
    \'title\' => \'LMS Princess Coronation 6229 Duchess of Hamilton\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'a7f1f7e4-1bca-bbd4-c164-35ce37a852c9\',
    \'nid\' => \'1\',
    \'type\' => \'locomotive\',
    \'language\' => \'und\',
    \'created\' => \'1332367758\',
    \'changed\' => \'1332367758\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'2c7e08d7-0bdf-1e94-edd3-781949165e96\',
    \'revision_timestamp\' => \'1332367758\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>6229 was built in 1938 at Crewe as the tenth member of its class and the last in the second batch of five red streamliners, complete with gold speed cheat stripes (the original five 6220-4 having been given a unique Caledonian blue livery with silver stripes). In 1939 no. 6229 swapped identities with the first of the class 6220 Coronation and was sent to North America with a specially-constructed Coronation Scot train to appear at the 1939 New York World&#39;s Fair.[1] There was therefore for a while a blue 6229 Duchess of Hamilton in the UK and a red 6220 Coronation in the USA. R.A. Riddles drove for most of the tour, owing to the illness of the assigned driver. The locomotive (though not its carriages) was shipped back from the States in 1942 after the outbreak of the Second World War, and the identities of the locomotives were swapped back in 1943.</p><p>6229 was painted wartime black livery in November 1944. Her streamlined casing was removed for maintenance-efficiency reasons in December 1947 and she was then given the LMS 1946 black livery. In 1948 she passed into BR ownership. BR added 40000 to her number to become 46229 on 15 April 1948. She was painted in the short-lived BR blue livery in April 1950, but was soon repainted on 26 April 1952 into Brunswick green. The semi-streamlined smokebox was replaced with a round-topped smokebox in February 1957, and in September 1958 she was painted maroon. The lining was BR style to begin with; then in October 1959 she received the current LMS style lining which she has carried for all her years in preservation.</p>\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>6229 was built in 1938 at Crewe as the tenth member of its class and the last in the second batch of five red streamliners, complete with gold speed cheat stripes (the original five 6220-4 having been given a unique Caledonian blue livery with silver stripes). In 1939 no. 6229 swapped identities with the first of the class 6220 Coronation and was sent to North America with a specially-constructed Coronation Scot train to appear at the 1939 New York World&#39;s Fair.[1] There was therefore for a while a blue 6229 Duchess of Hamilton in the UK and a red 6220 Coronation in the USA. R.A. Riddles drove for most of the tour, owing to the illness of the assigned driver. The locomotive (though not its carriages) was shipped back from the States in 1942 after the outbreak of the Second World War, and the identities of the locomotives were swapped back in 1943.</p>
<p>6229 was painted wartime black livery in November 1944. Her streamlined casing was removed for maintenance-efficiency reasons in December 1947 and she was then given the LMS 1946 black livery. In 1948 she passed into BR ownership. BR added 40000 to her number to become 46229 on 15 April 1948. She was painted in the short-lived BR blue livery in April 1950, but was soon repainted on 26 April 1952 into Brunswick green. The semi-streamlined smokebox was replaced with a round-topped smokebox in February 1957, and in September 1958 she was painted maroon. The lining was BR style to begin with; then in October 1959 she received the current LMS style lining which she has carried for all her years in preservation.</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_loco_class\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'Princess Coronation\',
          \'format\' => NULL,
          \'safe_value\' => \'Princess Coronation\',
        ),
      ),
    ),
    \'field_loco_company\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'LMS\',
          \'format\' => NULL,
          \'safe_value\' => \'LMS\',
        ),
      ),
    ),
    \'field_loco_name\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'Duchess of Hamilton\',
          \'format\' => NULL,
          \'safe_value\' => \'Duchess of Hamilton\',
        ),
      ),
    ),
    \'field_loco_number\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'6229\',
          \'format\' => NULL,
          \'safe_value\' => \'6229\',
        ),
      ),
    ),
    \'field_loco_power_type\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'steam\',
        ),
      ),
    ),
    \'field_loco_wheel_arrangement\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'4-6-2\',
          \'format\' => NULL,
          \'safe_value\' => \'4-6-2\',
        ),
      ),
    ),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332367758\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clydeh\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'2\',
      \'source\' => \'node/1\',
      \'alias\' => \'content/lms-princess-coronation-6229-duchess-hamilton\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
    array(
    \'vid\' => \'2\',
    \'uid\' => \'1\',
    \'title\' => \'GNR N2 1744\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'10cca534-02c7-2154-8dc8-c81ba57267fa\',
    \'nid\' => \'2\',
    \'type\' => \'locomotive\',
    \'language\' => \'und\',
    \'created\' => \'1332367758\',
    \'changed\' => \'1332367758\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'45129581-0994-d2a4-9516-ca9dcee250a3\',
    \'revision_timestamp\' => \'1332367758\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>The Great Northern Railway (GNR) Class N2 is an 0-6-2T side tank steam locomotive designed by Nigel Gresley and introduced in 1920. Further batches were built by the London and North Eastern Railway from 1925. They had superheaters and piston valves driven by Stephenson valve gear.</p>
<p>Some locomotives were fitted with condensing apparatus for working on the Metropolitan Railway Widened Lines between King\\\'s Cross and Moorgate.</p>\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>The Great Northern Railway (GNR) Class N2 is an 0-6-2T side tank steam locomotive designed by Nigel Gresley and introduced in 1920. Further batches were built by the London and North Eastern Railway from 1925. They had superheaters and piston valves driven by Stephenson valve gear.</p>
<p>Some locomotives were fitted with condensing apparatus for working on the Metropolitan Railway Widened Lines between King\\\'s Cross and Moorgate.</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_loco_class\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'N2\',
          \'format\' => NULL,
          \'safe_value\' => \'N2\',
        ),
      ),
    ),
    \'field_loco_company\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'Great Northern Railway\',
          \'format\' => NULL,
          \'safe_value\' => \'Great Northern Railway\',
        ),
      ),
    ),
    \'field_loco_name\' => array(),
    \'field_loco_number\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'1744\',
          \'format\' => NULL,
          \'safe_value\' => \'1744\',
        ),
      ),
    ),
    \'field_loco_power_type\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'steam\',
        ),
      ),
    ),
    \'field_loco_wheel_arrangement\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'0-6-2T\',
          \'format\' => NULL,
          \'safe_value\' => \'0-6-2T\',
        ),
      ),
    ),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332367758\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clydeh\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'3\',
      \'source\' => \'node/2\',
      \'alias\' => \'content/gnr-n2-1744\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
    array(
    \'vid\' => \'3\',
    \'uid\' => \'1\',
    \'title\' => \'BR Class 31 31018\',
    \'log\' => \'\',
    \'status\' => \'1\',
    \'comment\' => \'2\',
    \'promote\' => \'1\',
    \'sticky\' => \'0\',
    \'vuuid\' => \'b34a8c81-bb2c-67a4-a986-073c9de58cdb\',
    \'nid\' => \'3\',
    \'type\' => \'locomotive\',
    \'language\' => \'und\',
    \'created\' => \'1332367758\',
    \'changed\' => \'1332367758\',
    \'tnid\' => \'0\',
    \'translate\' => \'0\',
    \'uuid\' => \'9bfc55e0-266c-1174-9195-078d041da0e6\',
    \'revision_timestamp\' => \'1332367758\',
    \'revision_uid\' => \'1\',
    \'body\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'<p>The Class 31 was one of the Pilot Scheme locomotives ordered by British Railways to replace steam traction. They were originally built with Mirrlees JVS12T 1,250 bhp (930 kW) and 1,365 bhp (1,018 kW) engines and Brush electrical equipment, but the engines were not successful and in 1964 a programme of works commenced to re-engine the fleet with the 1,470 bhp (1,100 kW) English Electric 12SVT engines. The locomotives were originally known as Class 30 under TOPS, with re-engined examples joining Class 31. The class was originally intended for service on the Eastern Region, but gradually became common in both the Western and London Midland regions too.</p>\',
          \'summary\' => \'\',
          \'format\' => \'wysiwyg_editor\',
          \'safe_value\' => \' <p>The Class 31 was one of the Pilot Scheme locomotives ordered by British Railways to replace steam traction. They were originally built with Mirrlees JVS12T 1,250 bhp (930 kW) and 1,365 bhp (1,018 kW) engines and Brush electrical equipment, but the engines were not successful and in 1964 a programme of works commenced to re-engine the fleet with the 1,470 bhp (1,100 kW) English Electric 12SVT engines. The locomotives were originally known as Class 30 under TOPS, with re-engined examples joining Class 31. The class was originally intended for service on the Eastern Region, but gradually became common in both the Western and London Midland regions too.</p>
 \',
          \'safe_summary\' => \'  \',
        ),
      ),
    ),
    \'field_loco_class\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'31\',
          \'format\' => NULL,
          \'safe_value\' => \'31\',
        ),
      ),
    ),
    \'field_loco_company\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'BR\',
          \'format\' => NULL,
          \'safe_value\' => \'BR\',
        ),
      ),
    ),
    \'field_loco_name\' => array(),
    \'field_loco_number\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'31018\',
          \'format\' => NULL,
          \'safe_value\' => \'31018\',
        ),
      ),
    ),
    \'field_loco_power_type\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'diesel-electric\',
        ),
      ),
    ),
    \'field_loco_wheel_arrangement\' => array(
      \'und\' => array(
        \'0\' => array(
          \'value\' => \'A1A-A1A\',
          \'format\' => NULL,
          \'safe_value\' => \'A1A-A1A\',
        ),
      ),
    ),
    \'metatags\' => array(),
    \'rdf_mapping\' => array(
      \'rdftype\' => array(
        \'0\' => \'sioc:Item\',
        \'1\' => \'foaf:Document\',
      ),
      \'title\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:title\',
        ),
      ),
      \'created\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:date\',
          \'1\' => \'dc:created\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'changed\' => array(
        \'predicates\' => array(
          \'0\' => \'dc:modified\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
      \'body\' => array(
        \'predicates\' => array(
          \'0\' => \'content:encoded\',
        ),
      ),
      \'uid\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:has_creator\',
        ),
        \'type\' => \'rel\',
      ),
      \'name\' => array(
        \'predicates\' => array(
          \'0\' => \'foaf:name\',
        ),
      ),
      \'comment_count\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:num_replies\',
        ),
        \'datatype\' => \'xsd:integer\',
      ),
      \'last_activity\' => array(
        \'predicates\' => array(
          \'0\' => \'sioc:last_activity_date\',
        ),
        \'datatype\' => \'xsd:dateTime\',
        \'callback\' => \'date_iso8601\',
      ),
    ),
    \'cid\' => \'0\',
    \'last_comment_timestamp\' => \'1332367758\',
    \'last_comment_name\' => NULL,
    \'last_comment_uid\' => \'1\',
    \'comment_count\' => \'0\',
    \'name\' => \'clydeh\',
    \'picture\' => \'0\',
    \'data\' => \'b:0;\',
    \'path\' => array(
      \'pid\' => \'4\',
      \'source\' => \'node/3\',
      \'alias\' => \'content/br-class-31-31018\',
      \'language\' => \'und\',
    ),
    \'menu\' => array(
      \'link_title\' => \'\',
      \'mlid\' => 0,
      \'plid\' => 0,
      \'menu_name\' => \'main-menu\',
      \'weight\' => 0,
      \'options\' => array(),
      \'module\' => \'menu\',
      \'expanded\' => 0,
      \'hidden\' => 0,
      \'has_children\' => 0,
      \'customized\' => 0,
      \'parent_depth_limit\' => 8,
      \'description\' => \'\',
      \'enabled\' => 1,
    ),
    \'node_export_drupal_version\' => \'7\',
    \'#_export_node_encode_object\' => \'1\',
  ),
)',
);
  return $node_export;
}
